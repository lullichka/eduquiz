package com.daoris.eduquiz;

import android.app.Application;

import com.daoris.eduquiz.data.APIInterface;
import com.google.firebase.database.FirebaseDatabase;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class EduQuizApp extends Application{

    private static APIInterface service;

    public void onCreate() {
        super.onCreate();
        Realm.init(getApplicationContext());
        RealmConfiguration config = new RealmConfiguration.Builder()
                .name("database")
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(config);
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        //todo add API interface here
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://www.google.com")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        service = retrofit.create(APIInterface.class);
    }
    public static APIInterface getApi(){
        return service;
    }
}
