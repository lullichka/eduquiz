package com.daoris.eduquiz.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.daoris.eduquiz.R;
import com.daoris.eduquiz.data.DownloadInterface;
import com.daoris.eduquiz.data.FileDownloadUtil;
import com.daoris.eduquiz.model.Category;

import io.realm.Realm;
import io.realm.RealmResults;

public class SettingsActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener,
        View.OnClickListener{
    private static final String APP_PREFERENCES = "settings";
    private static final String USER_NAME = "username";
    private static final String LANGUAGE = "language";
    private static final String CHEKH = "čeština";
    private static final String POLSKI= "polski";
    private static final String ENGLISH = "english";
    private static final String TAG = SettingsActivity.class.getSimpleName();
    private static final String DESCRIPTION = "/description.mp3";
    private static final String RIGHT = "/right.mp3";
    private static final String WRONG = "/wrong.mp3";

    private SharedPreferences mSettings;
    private String mSelectedLanguage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        Spinner spinner = (Spinner) findViewById(R.id.spinner);
        TextView tvNick = (TextView) findViewById(R.id.nick);
        TextView registered = (TextView) findViewById(R.id.registered);
        TextView registered2 = (TextView) findViewById(R.id.registered2);
        Button mButton = (Button) findViewById(R.id.button);
        Button mButtonUpdate = (Button) findViewById(R.id.update);
        mSettings = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
        String username = mSettings.getString(USER_NAME, null);
        String language = mSettings.getString(LANGUAGE, null);
        tvNick.setText(username);
        switch (language){
            case ENGLISH:
                registered.setText(getResources().getString(R.string.registered));
                registered2.setText(getResources().getString(R.string.registered2));
                break;
            case POLSKI:
                registered.setText(getResources().getString(R.string.registered_polski));
                registered2.setText(getResources().getString(R.string.registered2_polski));
                break;
            case CHEKH:
                registered.setText(getResources().getString(R.string.registered_chekh));
                registered2.setText(getResources().getString(R.string.registered2_chekh));
                break;
        }
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.languages, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);
        mButton.setOnClickListener(this);
        mButtonUpdate.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id){
            case R.id.button:
                finish();
                break;
            case R.id.update:
                update();
                break;
        }
    }

    private void update() {
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putString(LANGUAGE, mSelectedLanguage.toLowerCase());
        editor.commit();

        Realm realm = Realm.getDefaultInstance();
        final RealmResults<Category> savedList = realm.where(Category.class).findAll();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                savedList.deleteAllFromRealm();
            }
        });
        FileDownloadUtil.downloadCommonFiles(getApplicationContext(), DESCRIPTION);
        FileDownloadUtil.downloadCommonFiles(getApplicationContext(), RIGHT);
        FileDownloadUtil.downloadCommonFiles(getApplicationContext(), WRONG);
        FileDownloadUtil.getListOfCategories(getApplicationContext(), new DownloadInterface.LoadFilesCallback() {
            @Override
            public void onFilesLoaded() {
                Log.d(TAG, "Sucessfully loaded");
                Intent intent = new Intent(SettingsActivity.this, WelcomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);
                finish();
            }
        });

    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        mSelectedLanguage = (String) adapterView.getItemAtPosition(i);
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
        mSelectedLanguage = mSettings.getString(LANGUAGE, getResources().getString(R.string.default_language));
    }
}
