package com.daoris.eduquiz.data;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;

import com.daoris.eduquiz.activities.MainActivity;
import com.daoris.eduquiz.model.Category;

import io.realm.Realm;


public class DownloadCompleteReceiver extends BroadcastReceiver {
    public static final String ACTION_DOWNLOAD = "com.daoris.eduquiz.data.action.download";
    public static final String CATEGORY = "category";
    public static final String NUMBER_OF_QUESTION = "number_of_questions";
    public static final String TAG = DownloadCompleteReceiver.class.getSimpleName();
    private final Handler handler;


    public DownloadCompleteReceiver(Handler handler) {
        this.handler = handler;
    }

    @Override
    public void onReceive(final Context context, final Intent intent) {

        String category = intent.getStringExtra(CATEGORY);
        String currentnumber = intent.getStringExtra(NUMBER_OF_QUESTION);
        Realm realm = Realm.getDefaultInstance();
        final Category category1 = realm.where(Category.class)
                .equalTo("alias", category)
                .findFirst();

        if (Integer.parseInt(currentnumber) < category1.getNumQuest()) {
            Intent downloadIntent = new Intent(context, DownloadIntentService.class);
            downloadIntent.putExtra(CATEGORY, category1.getAlias());
            downloadIntent.putExtra(NUMBER_OF_QUESTION, String.valueOf(Integer.parseInt(currentnumber) + 1));
            context.startService(downloadIntent);
        } else {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    if(MainActivity.getInstance()!=null)
                        MainActivity.getInstance().updateUI();
                }
            });
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    category1.setDownloaded(true);

                }
            });
            Category category2 = realm.where(Category.class)
                    .equalTo("downloaded", false)
                    .findFirst();
            if (category2 != null) {
                Intent downloadIntent1 = new Intent(context, DownloadIntentService.class);
                downloadIntent1.putExtra(CATEGORY, category2.getAlias());
                downloadIntent1.putExtra(NUMBER_OF_QUESTION, "1");
                context.startService(downloadIntent1);
            }
        }
    }
}
