package com.daoris.eduquiz.graphics;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.View;

/* Class which draws colored shadows for grayscaled images in FragmentQuestion */

public class AnswerDragShadowBuilder extends View.DragShadowBuilder {
    private Drawable shadow;

    public AnswerDragShadowBuilder(View v) {
        super(v);
    }

    public static View.DragShadowBuilder fromBitmap(Context context, Bitmap bmp, View v) {
        if (bmp == null) {
            throw new IllegalArgumentException("Bitmap cannot be null");
        }
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        AnswerDragShadowBuilder builder = new AnswerDragShadowBuilder(v);
        Bitmap c = transform(bmp, 20);
        builder.shadow = new BitmapDrawable(context.getResources(), c);
        builder.shadow.setBounds(0, 0, builder.shadow.getMinimumWidth(), builder.shadow.getMinimumHeight());
        return builder;
    }

    @Override
    public void onDrawShadow(Canvas canvas) {
        shadow.draw(canvas);
    }

    @Override
    public void onProvideShadowMetrics(Point shadowSize, Point shadowTouchPoint) {
        shadowSize.x = shadow.getMinimumWidth();
        shadowSize.y = shadow.getMinimumHeight();

        shadowTouchPoint.x = (shadowSize.x / 2);
        shadowTouchPoint.y = (shadowSize.y / 2);
    }

    //method for shadows rounded corners
    private static Bitmap transform(Bitmap source, int pixels) {

        Bitmap bitmap = Bitmap.createBitmap(source.getWidth(), source.getHeight(), source.getConfig());
        Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);

        canvas.drawRoundRect(rectF, (float) pixels, (float) pixels, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(source, rect, rect, paint);
        if (bitmap != source) {
            source.recycle();
        }
        return bitmap;
    }
}
