package com.daoris.eduquiz.fragments;

import android.content.ClipData;
import android.content.ClipDescription;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.TransitionDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.daoris.eduquiz.R;
import com.daoris.eduquiz.data.FileDownloadUtil;
import com.daoris.eduquiz.graphics.AnswerDragShadowBuilder;
import com.daoris.eduquiz.graphics.GrayscaleTransformation;
import com.daoris.eduquiz.model.UserLog;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.TimeUnit;


public class FragmentQuestion extends Fragment implements View.OnClickListener, View.OnTouchListener,
        View.OnDragListener {

    private static final String TAG = FragmentQuestion.class.getSimpleName();
    private static final String APP_PREFERENCES = "settings";
    private static final String LANGUAGE = "language";
    private static final String USER_ID = "uniqueId";
    private static final String USER_NAME = "username";

    private static final String ARG_CATEGORY = "CATEGORY";
    private static final String ARG_NUMBER = "QUESTION_NUMBER";
    private static final String ARG_RANDOM = "RANDOM_LAYOUT";
    private static final String QUESTION_JPG = "/question.jpg";
    private static final String ANS1_ANSWER_JPG = "/ans1/answer.jpg";
    private static final String ANS2_ANSWER_JPG = "/ans2/answer.jpg";
    private static final String ANS3_ANSWER_JPG = "/ans3/answer.jpg";
    private static final String QUESTION_MP3 = "/question.mp3";
    private static final String ANS1_ANSWER_MP3 = "/ans1/answer.mp3";
    private static final String ANS2_ANSWER_MP3 = "/ans2/answer.mp3";
    private static final String ANS3_ANSWER_MP3 = "/ans3/answer.mp3";
    private static final String WRONG_MP3 = "/wrong.mp3";
    private static final String RIGHT_MP3 = "/right.mp3";
    private static final String USERLOG_CHILD = "USERLOG";
    private static final int ANIMATION_TIME = 4000;
    private SharedPreferences mSettings;
    private OnFragmentInteractionListener mListener;
    private String category;
    private int mQuestionNum;
    private String mPathToFolder;
    private String mPathToCommonMp3;
    private int mRandom;
    private File mFileImgAnsTop;
    private File mFileImgAnsMiddle;
    private File mFileImgAnsBottom;
    private File mFileQuest;
    private String mOrder; // this will be used in Logs
    private long mOpeningTime;
    private int mSelectedAns;
    private String mPathTopMp3;
    private String mPathBottomMp3;
    private String mPathMiddleMp3;
    private String mPathToTopImg;
    private String mPathToMiddleImg;
    private String mPathToBottomImg;
    private MediaPlayer mMediaPlayer;
    private View mShade; //to disable listeners;

    public FragmentQuestion() {
        // Required empty public constructor
    }

    //argument random creates fragment with random order of answers
    public static FragmentQuestion newInstance(String category, int questionNumber) {
        Random random = new Random();
        FragmentQuestion fragment = new FragmentQuestion();
        Bundle args = new Bundle();
        args.putString(ARG_CATEGORY, category);
        args.putInt(ARG_NUMBER, questionNumber);
        args.putInt(ARG_RANDOM, random.nextInt(6) + 1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //getting category,  question number and random layout
        if (getArguments() != null) {
            category = getArguments().getString(ARG_CATEGORY);
            mQuestionNum = getArguments().getInt(ARG_NUMBER);
            mRandom = getArguments().getInt(ARG_RANDOM);
        }
        mSettings = getActivity().getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
        String selectedLanguage = mSettings.getString(LANGUAGE, getResources().getString(R.string.default_language).toLowerCase());
        mOpeningTime = System.currentTimeMillis();
        //getting all needed paths to files
        if (FileDownloadUtil.isExternalStorageAvailable() && !FileDownloadUtil.isExternalStorageReadOnly()) {
            mPathToCommonMp3 = "/" + selectedLanguage;
            mPathToFolder = "/" + selectedLanguage + "/" + category + "/" + mQuestionNum;
            mFileQuest = new File(getActivity().getExternalFilesDir(mPathToFolder), QUESTION_JPG);
            switch (mRandom) {
                case (1):
                    mFileImgAnsTop = new File(getActivity().getExternalFilesDir(mPathToFolder), ANS1_ANSWER_JPG);
                    mFileImgAnsMiddle = new File(getActivity().getExternalFilesDir(mPathToFolder), ANS2_ANSWER_JPG);
                    mFileImgAnsBottom = new File(getActivity().getExternalFilesDir(mPathToFolder), ANS3_ANSWER_JPG);
                    mPathTopMp3 = getActivity().getExternalFilesDir(mPathToFolder) + ANS1_ANSWER_MP3;
                    mPathMiddleMp3 = getActivity().getExternalFilesDir(mPathToFolder) + ANS2_ANSWER_MP3;
                    mPathBottomMp3 = getActivity().getExternalFilesDir(mPathToFolder) + ANS3_ANSWER_MP3;
                    mOrder = "123";
                    break;
                case (2):
                    mFileImgAnsTop = new File(getActivity().getExternalFilesDir(mPathToFolder), ANS1_ANSWER_JPG);
                    mFileImgAnsMiddle = new File(getActivity().getExternalFilesDir(mPathToFolder), ANS3_ANSWER_JPG);
                    mFileImgAnsBottom = new File(getActivity().getExternalFilesDir(mPathToFolder), ANS2_ANSWER_JPG);
                    mPathTopMp3 = getActivity().getExternalFilesDir(mPathToFolder) + ANS1_ANSWER_MP3;
                    mPathMiddleMp3 = getActivity().getExternalFilesDir(mPathToFolder) + ANS3_ANSWER_MP3;
                    mPathBottomMp3 = getActivity().getExternalFilesDir(mPathToFolder) + ANS2_ANSWER_MP3;
                    mOrder = "132";
                    break;
                case (3):
                    mFileImgAnsTop = new File(getActivity().getExternalFilesDir(mPathToFolder), ANS2_ANSWER_JPG);
                    mFileImgAnsMiddle = new File(getActivity().getExternalFilesDir(mPathToFolder), ANS1_ANSWER_JPG);
                    mFileImgAnsBottom = new File(getActivity().getExternalFilesDir(mPathToFolder), ANS3_ANSWER_JPG);
                    mPathTopMp3 = getActivity().getExternalFilesDir(mPathToFolder) + ANS2_ANSWER_MP3;
                    mPathMiddleMp3 = getActivity().getExternalFilesDir(mPathToFolder) + ANS1_ANSWER_MP3;
                    mPathBottomMp3 = getActivity().getExternalFilesDir(mPathToFolder) + ANS3_ANSWER_MP3;
                    mOrder = "213";
                    break;
                case (4):
                    mFileImgAnsTop = new File(getActivity().getExternalFilesDir(mPathToFolder), ANS2_ANSWER_JPG);
                    mFileImgAnsMiddle = new File(getActivity().getExternalFilesDir(mPathToFolder), ANS3_ANSWER_JPG);
                    mFileImgAnsBottom = new File(getActivity().getExternalFilesDir(mPathToFolder), ANS1_ANSWER_JPG);
                    mPathTopMp3 = getActivity().getExternalFilesDir(mPathToFolder) + ANS2_ANSWER_MP3;
                    mPathMiddleMp3 = getActivity().getExternalFilesDir(mPathToFolder) + ANS3_ANSWER_MP3;
                    mPathBottomMp3 = getActivity().getExternalFilesDir(mPathToFolder) + ANS1_ANSWER_MP3;
                    mOrder = "231";
                    break;
                case (5):
                    mFileImgAnsTop = new File(getActivity().getExternalFilesDir(mPathToFolder), ANS3_ANSWER_JPG);
                    mFileImgAnsMiddle = new File(getActivity().getExternalFilesDir(mPathToFolder), ANS1_ANSWER_JPG);
                    mFileImgAnsBottom = new File(getActivity().getExternalFilesDir(mPathToFolder), ANS2_ANSWER_JPG);
                    mPathTopMp3 = getActivity().getExternalFilesDir(mPathToFolder) + ANS3_ANSWER_MP3;
                    mPathMiddleMp3 = getActivity().getExternalFilesDir(mPathToFolder) + ANS1_ANSWER_MP3;
                    mPathBottomMp3 = getActivity().getExternalFilesDir(mPathToFolder) + ANS2_ANSWER_MP3;
                    mOrder = "312";
                    break;
                case (6):
                    mFileImgAnsTop = new File(getActivity().getExternalFilesDir(mPathToFolder), ANS3_ANSWER_JPG);
                    mFileImgAnsMiddle = new File(getActivity().getExternalFilesDir(mPathToFolder), ANS2_ANSWER_JPG);
                    mFileImgAnsBottom = new File(getActivity().getExternalFilesDir(mPathToFolder), ANS1_ANSWER_JPG);
                    mPathTopMp3 = getActivity().getExternalFilesDir(mPathToFolder) + ANS3_ANSWER_MP3;
                    mPathMiddleMp3 = getActivity().getExternalFilesDir(mPathToFolder) + ANS2_ANSWER_MP3;
                    mPathBottomMp3 = getActivity().getExternalFilesDir(mPathToFolder) + ANS1_ANSWER_MP3;
                    mOrder = "321";
                    break;
            }
            mPathToTopImg = String.valueOf(mFileImgAnsTop);
            mPathToMiddleImg = String.valueOf(mFileImgAnsMiddle);
            mPathToBottomImg = String.valueOf(mFileImgAnsBottom);
        } else {
            Toast.makeText(getActivity(), "External storage must be accessible", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_question, container, false);
        ImageView imageQuestion = (ImageView) view.findViewById(R.id.imageQuestion);
        ImageView topImgAnswer = (ImageView) view.findViewById(R.id.top);
        ImageView middleImgAnswer = (ImageView) view.findViewById(R.id.middle);
        ImageView bottomImgAnswer = (ImageView) view.findViewById(R.id.bottom);
        LinearLayout layoutQuestion = (LinearLayout) view.findViewById(R.id.layoutQuestion);
        layoutQuestion.setOnClickListener(this);
        mShade = view.findViewById(R.id.shade);
        mShade.setOnClickListener(this);
        Picasso.with(getActivity())
                .load(mFileQuest)
                .fit()
                .into(imageQuestion);
        Picasso.with(getActivity())
                .load(mFileImgAnsTop)
                .transform(new GrayscaleTransformation(Picasso.with(getActivity())))
                .fit()
                .into(topImgAnswer);
        Picasso.with(getActivity())
                .load(mFileImgAnsMiddle)
                .transform(new GrayscaleTransformation(Picasso.with(getActivity())))
                .fit()
                .into(middleImgAnswer);
        Picasso.with(getActivity())
                .load(mFileImgAnsBottom)
                .transform(new GrayscaleTransformation(Picasso.with(getActivity())))
                .fit()
                .into(bottomImgAnswer);
        topImgAnswer.setOnTouchListener(this);
        middleImgAnswer.setOnTouchListener(this);
        bottomImgAnswer.setOnTouchListener(this);
        layoutQuestion.setOnDragListener(this);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        releaseMP();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.layoutQuestion:
                releaseMP();
                String filePathToQuestion = getActivity().getExternalFilesDir(mPathToFolder) + QUESTION_MP3;
                playMedia(filePathToQuestion);
                break;
            case R.id.shade:
                //Toast.makeText(getContext(), "Shade", Toast.LENGTH_LONG).show();
                break;
        }

    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        Bitmap colored = null;
        String filePath = null;
        if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
            switch (view.getId()) {
                case R.id.top:
                    releaseMP();
                    colored = BitmapFactory.decodeFile(mPathToTopImg);
                    filePath = mPathTopMp3;
                    break;
                case R.id.middle:
                    releaseMP();
                    colored = BitmapFactory.decodeFile(mPathToMiddleImg);
                    filePath = mPathMiddleMp3;
                    break;
                case R.id.bottom:
                    releaseMP();
                    colored = BitmapFactory.decodeFile(mPathToBottomImg);
                    filePath = mPathBottomMp3;
                    break;
            }
            View.DragShadowBuilder shadowBuilder = AnswerDragShadowBuilder.fromBitmap(getActivity(), colored, view);

            ClipData.Item item = new ClipData.Item(filePath);
            ClipData dragData = new ClipData(
                    filePath,
                    new String[]{ClipDescription.MIMETYPE_TEXT_PLAIN}, item
            );
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                view.startDragAndDrop(dragData, shadowBuilder, view, 0);
            } else {
                view.startDrag(dragData, shadowBuilder, view, 0);
            }
            playMedia(filePath);
            colored.recycle();
            return true;
        } else
            return false;
    }

    //method for playing MP3 files
    private void playMedia(String filePath) {
        mMediaPlayer = MediaPlayer.create(getActivity(), Uri.parse("file://" + filePath));
        mMediaPlayer.start();
        mShade.setVisibility(View.VISIBLE);
        mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mp.stop();
                mp.release();
                mShade.setVisibility(View.GONE);
            }
        });
    }

    private void releaseMP() {
        if (mMediaPlayer != null) {
            try {
                mMediaPlayer.release();
                mMediaPlayer = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean onDrag(View view, DragEvent dragEvent) {
        int action = dragEvent.getAction();
        switch (action) {
            case DragEvent.ACTION_DRAG_STARTED:
                break;
            case DragEvent.ACTION_DRAG_EXITED:
                break;
            case DragEvent.ACTION_DROP:
                LinearLayout container = (LinearLayout) view.getParent();
                ClipData.Item item = dragEvent.getClipData().getItemAt(0);
                String dragData = (String) item.getText();
                if (dragData.endsWith(ANS1_ANSWER_MP3)) {
                    mSelectedAns = 1;
                } else if (dragData.endsWith(ANS2_ANSWER_MP3)) {
                    mSelectedAns = 2;
                } else if (dragData.endsWith(ANS3_ANSWER_MP3)) {
                    mSelectedAns = 3;
                }
                if (mSelectedAns == 1) {
                    ColorDrawable[] color = {new ColorDrawable(Color.GREEN), new ColorDrawable(Color.LTGRAY)};
                    TransitionDrawable trans = new TransitionDrawable(color);
                    container.setBackground(trans);
                    trans.startTransition(3000);
                    String filePathToRight = getActivity().getExternalFilesDir(mPathToCommonMp3) + RIGHT_MP3;
                    playMedia(filePathToRight);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mListener.onFragmentInteraction();
                        }
                    }, ANIMATION_TIME);
                } else {
                    ColorDrawable[] color = {new ColorDrawable(Color.RED), new ColorDrawable(Color.LTGRAY)};
                    TransitionDrawable trans = new TransitionDrawable(color);
                    container.setBackground(trans);
                    trans.startTransition(3000);
                    String filePathToWrong = getActivity().getExternalFilesDir(mPathToCommonMp3) + WRONG_MP3;
                    playMedia(filePathToWrong);
                }
                pushUserLogToDatabase();
                break;
            default:
                break;
        }
        return true;
    }

    //method which writes userlog to Firebase
    private void pushUserLogToDatabase() {
        long passedTime = System.currentTimeMillis() - mOpeningTime;
        Date date = new Date(mOpeningTime);
        SimpleDateFormat ft = new SimpleDateFormat("yyyy.MM.dd 'at' hh:mm:ss a zzz");
        long seconds = TimeUnit.MILLISECONDS.toSeconds(passedTime);
        String passedTimeInSec = String.valueOf(seconds) + " seconds";
        UserLog userLog = new UserLog(mSettings.getString(USER_ID, null));
        userLog.setUsername(mSettings.getString(USER_NAME, null));
        userLog.setOpenTime(ft.format(date));
        userLog.setQuestionId(category + "/" + mQuestionNum);
        userLog.setTimePassed(passedTimeInSec);
        userLog.setOrder(mOrder);
        userLog.setSelectedAns(mSelectedAns);
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference(USERLOG_CHILD);
        DatabaseReference reference = databaseReference.push();
        reference.setValue(userLog);
    }


    public interface OnFragmentInteractionListener {
        void onFragmentInteraction();
    }
}