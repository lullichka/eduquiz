package com.daoris.eduquiz.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;

import com.daoris.eduquiz.fragments.FragmentQuestion;
import com.daoris.eduquiz.R;

public class QuestionActivity extends AppCompatActivity implements FragmentQuestion.OnFragmentInteractionListener {

    private String TAG = QuestionActivity.class.getSimpleName();

    public static final String CATEGORY = "category";
    public static final String NUMBER_OF_QUESTIONS = "numQuest";
    String mCategory = null;
    int mNumberOfQuest;
    int mNumber = 1;
    private GestureDetector mGestureDetector;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question);
        Intent intent = getIntent();
        mCategory = intent.getStringExtra(CATEGORY);
        mNumberOfQuest = intent.getIntExtra(NUMBER_OF_QUESTIONS, 0);
        // Create an object of the Android_Gesture_Detector  Class
        MyGestureDetector gestureDetector = new MyGestureDetector();
        // Create a GestureDetector
        mGestureDetector = new GestureDetector(this, gestureDetector);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.contentFrame, FragmentQuestion.newInstance(mCategory, mNumber)).commit();
        Log.d(TAG, mCategory + String.valueOf(mNumberOfQuest));

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return mGestureDetector.onTouchEvent(event);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent e) {
        mGestureDetector.onTouchEvent(e);
        return super.dispatchTouchEvent(e);
    }

    @Override
    public void onFragmentInteraction() {
        ++mNumber;
        if (mNumber < mNumberOfQuest + 1) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.contentFrame,
                            FragmentQuestion.newInstance(mCategory, mNumber)).commit();
        } else {
            finish();

        }
    }

    public void onSwipeTop() {
        ++mNumber;
        if (mNumber < mNumberOfQuest + 1) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.contentFrame,
                            FragmentQuestion.newInstance(mCategory, mNumber)).commit();
        } else {
            finish();
        }
    }

    public void onSwipeBottom() {
        --mNumber;
        if (mNumber < 1) {
            finish();
        } else {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.contentFrame,
                            FragmentQuestion.newInstance(mCategory, mNumber)).commit();
        }
    }

    class MyGestureDetector extends android.view.GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            float diffHorizontal = e1.getX() - e2.getX();
            float diffVertical = e1.getY() - e2.getY();
            if (e1.getX() < e2.getX() && Math.abs(diffHorizontal) > Math.abs(diffVertical)) {
                finish();
            }
            if (e1.getY() < e2.getY() && Math.abs(diffVertical) > Math.abs(diffHorizontal)) {
                onSwipeBottom();
            }
            if (e1.getY() > e2.getY() && Math.abs(diffVertical) > Math.abs(diffHorizontal)) {
                onSwipeTop();
            }
            return true;
        }
    }
}