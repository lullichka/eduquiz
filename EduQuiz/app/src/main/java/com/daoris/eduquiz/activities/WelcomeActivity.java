package com.daoris.eduquiz.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.daoris.eduquiz.R;
import com.daoris.eduquiz.data.DownloadInterface;
import com.daoris.eduquiz.data.FileDownloadUtil;


public class WelcomeActivity extends AppCompatActivity {
  private static final String APP_PREFERENCES = "settings";
  private static final String USER_NAME = "username";
  private static final String LANGUAGE = "language";
  private static final String CHEKH = "čeština";
  private static final String POLSKI = "polski";
  private static final String ENGLISH = "english";


  private static final String TAG = WelcomeActivity.class.getSimpleName();
  private static int SPLASH_TIME_OUT = 3000;
  private boolean updateDialogShown;


  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_welcome);
    Button update = (Button) findViewById(R.id.btnUpdate);
    update.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        showUpdateDialog();
        updateDialogShown = true;
      }
    });
    TextView userName = (TextView) findViewById(R.id.nickname);
    TextView welcome = (TextView) findViewById(R.id.welcome);
    SharedPreferences settings = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
    String username = settings.getString(USER_NAME, null);
    String language = settings.getString(LANGUAGE, getApplicationContext().getResources().getString(R.string.default_language).toLowerCase());
    if (username == null) {
      Intent i = new Intent(this, LaunchingActivity.class);
      startActivity(i);
    } else {
      switch (language) {
        case ENGLISH:
          String text = String.format(getResources().getString(R.string.as_nickname), username);
          welcome.setText(getResources().getString(R.string.welcome));
          userName.setText(text);
          break;
        case POLSKI:
          String text1 = String.format(getResources().getString(R.string.as_nickname_polski), username);
          welcome.setText(getResources().getString(R.string.welcome_polski));
          userName.setText(text1);
          break;
        case CHEKH:
          String text2 = String.format(getResources().getString(R.string.as_nickname_chekh), username);
          welcome.setText(getResources().getString(R.string.welcome_chekh));
          userName.setText(text2);
          break;
      }
      userName.setVisibility(View.VISIBLE);

      new Handler().postDelayed(new Runnable() {

        @Override
        public void run() {
          if (!updateDialogShown) {
            Intent intent = new Intent(WelcomeActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
          }
        }
      }, SPLASH_TIME_OUT);
    }
  }

  private void showUpdateDialog() {
    final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
    alertDialogBuilder.setTitle(getString(R.string.actualizovat));
    alertDialogBuilder.setCancelable(true);
    final EditText input = new EditText(WelcomeActivity.this);
    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
        LinearLayout.LayoutParams.MATCH_PARENT,
        LinearLayout.LayoutParams.MATCH_PARENT);
    input.setLayoutParams(lp);
    alertDialogBuilder.setView(input);
    alertDialogBuilder.setPositiveButton(R.string.start,
        new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialogInterface, int i) {
            String password = input.getText().toString();
            if (!password.equals("E1963")) {
              showPasswordIncorrect();
            } else FileDownloadUtil.clearFilesAndDownloadAgain(WelcomeActivity.this,
                new DownloadInterface.FilesDeletedCallback() {
                  @Override
                  public void onFilesDeleted() {
                    FileDownloadUtil.getListOfCategories(WelcomeActivity.this,
                        new DownloadInterface.LoadFilesCallback() {
                          @Override
                          public void onFilesLoaded() {
                            Intent intent = new Intent(WelcomeActivity.this,
                                MainActivity.class);
                            startActivity(intent);
                            finish();
                          }
                        });
                  }
                });
          }
        });
    alertDialogBuilder.setNegativeButton(R.string.cancel,
        new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialogInterface, int i) {
            Intent intent = new Intent(WelcomeActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
          }
        });
    alertDialogBuilder.create();
    alertDialogBuilder.show();

  }

  private void showPasswordIncorrect() {
    final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
    alertDialogBuilder.setTitle(getString(R.string.passwordIncorrect));
    alertDialogBuilder.setCancelable(true);
    alertDialogBuilder.setPositiveButton(R.string.tryAgain,
        new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialogInterface, int i) {
            showUpdateDialog();
          }
        });
    alertDialogBuilder.setNegativeButton(R.string.cancel,
        new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialogInterface, int i) {
            Intent intent = new Intent(WelcomeActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
          }
        });
    alertDialogBuilder.create();
    alertDialogBuilder.show();
  }

}


