package com.daoris.eduquiz.adapter;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.daoris.eduquiz.R;
import com.daoris.eduquiz.activities.QuestionActivity;
import com.daoris.eduquiz.model.Category;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

/* Adapter for RecyclerView in Main Activity */

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder> {

    private static final String TAG = CategoryAdapter.class.getSimpleName();
    private static final String APP_PREFERENCES = "settings";
    private static final String LANGUAGE = "language";

    private List<Category> mCategoriesList;
    private final Context mContext;

    public CategoryAdapter(Context context, List<Category> categories) {
        setList(categories);
        mCategoriesList = categories;
        mContext = context;
    }
    @Override
    public long getItemId(int position) {
        return mCategoriesList.get(position).getId();
    }

    private void setList(List<Category> categories) {
        mCategoriesList = categories;
    }

    public void replaceData(List<Category> categories) {
        setList(categories);
        notifyDataSetChanged();
    }

    @Override
    public CategoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View categoryView = inflater.inflate(R.layout.main_category_list_item, parent, false);
        return new CategoryViewHolder(categoryView);
    }

    @Override
    public void onBindViewHolder(CategoryViewHolder holder, int position) {
        Category category = mCategoriesList.get(position);
        holder.categoryDescription.setText(category.getName());
        holder.numberOfQuestions.setText(String.valueOf(category.getNumQuest()));
        holder.numberOfQuestions.bringToFront();
        String filename = "image_category.jpg";
        SharedPreferences settings = mContext.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
        String selectedLanguage = settings.getString(LANGUAGE,
                mContext.getResources().getString(R.string.default_language).toLowerCase());
        String filepathToFolder = selectedLanguage.concat("/").concat(category.getAlias());
        File categoryImageFile = new File(mContext.getExternalFilesDir(filepathToFolder), filename);
        Picasso.with(mContext)
                .load(categoryImageFile)
                .fit()
                .placeholder(R.drawable.get_dressed)
                .into(holder.imageCategory);
    }


    @Override
    public int getItemCount() {
        return mCategoriesList == null ? 0 : mCategoriesList.size();
    }

    class CategoryViewHolder extends RecyclerView.ViewHolder {
        ImageView imageCategory = (ImageView) itemView.findViewById(R.id.categoryImage);
        TextView numberOfQuestions = (TextView) itemView.findViewById(R.id.numberOfQuestions);
        TextView categoryDescription = (TextView) itemView.findViewById(R.id.tvDescription);

        CategoryViewHolder(View itemView) {
            super(itemView);
            setIsRecyclable(false);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    Category category = getItem(position);
                    Intent intent = new Intent(mContext, QuestionActivity.class);
                    if (category.getNumQuest() != 0) {
                        intent.putExtra(QuestionActivity.CATEGORY, category.getAlias().toLowerCase());
                        intent.putExtra(QuestionActivity.NUMBER_OF_QUESTIONS, category.getNumQuest());
                        Log.d(TAG, intent.getStringExtra(QuestionActivity.CATEGORY));
                        mContext.startActivity(intent);
                    } else
                        Toast.makeText(mContext, "Sorry, chosen category has no questions!", Toast.LENGTH_LONG).show();
                }
            });
        }

        Category getItem(int position) {
            return mCategoriesList.get(position);
        }
    }
}
