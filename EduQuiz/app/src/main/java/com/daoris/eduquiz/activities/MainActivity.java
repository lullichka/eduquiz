package com.daoris.eduquiz.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.daoris.eduquiz.R;
import com.daoris.eduquiz.adapter.CategoryAdapter;
import com.daoris.eduquiz.data.DownloadCompleteReceiver;
import com.daoris.eduquiz.data.DownloadIntentService;
import com.daoris.eduquiz.graphics.ItemOffsetDecoration;
import com.daoris.eduquiz.model.Category;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmQuery;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    private static final String APP_PREFERENCES = "settings";
    private static final String LANGUAGE = "language";
    public static final String CATEGORY = "category";
    public static final String NUMBER_OF_QUESTION = "number_of_questions";


    private CategoryAdapter mCategoriesAdapter;
    private String mSelectedLanguage;
    private DownloadCompleteReceiver receiver;
    private static MainActivity mainActivityRunningInstance;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mainActivityRunningInstance = this;
        Realm realm = Realm.getDefaultInstance();
        Category category = realm.where(Category.class)
                .equalTo("downloaded", false)
                .findFirst();
        if (category != null) {
            Intent downloadIntent = new Intent(MainActivity.this, DownloadIntentService.class);
            downloadIntent.putExtra(CATEGORY, category.getAlias());
            downloadIntent.putExtra(NUMBER_OF_QUESTION, String.valueOf(category.getLastDownloadedQuestion() + 1));
            startService(downloadIntent);
            Log.d(TAG, "Service fired");
        }
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.downloading));
        progressDialog.setIndeterminate(true);
        progressDialog.show();
        IntentFilter filter = new IntentFilter(DownloadCompleteReceiver.ACTION_DOWNLOAD);
        filter.addCategory(Intent.CATEGORY_DEFAULT);
        receiver = new DownloadCompleteReceiver(new Handler());
        registerReceiver(receiver, filter);
        mCategoriesAdapter = new CategoryAdapter(this, new ArrayList<Category>(0));
        SharedPreferences preferences = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
        mSelectedLanguage = preferences.getString(LANGUAGE, null);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.categoryRecyclerView);
        recyclerView.setAdapter(mCategoriesAdapter);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2, GridLayoutManager.HORIZONTAL, false);
        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(getApplicationContext(), R.dimen.item_offset);
        recyclerView.addItemDecoration(itemDecoration);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setNestedScrollingEnabled(true);
        if (getExternalFilesDir(mSelectedLanguage) != null) {
            mCategoriesAdapter.replaceData(listDownloadedCategories());
        } else {
            Toast.makeText(this, "Check external storage and update Google play services", Toast.LENGTH_LONG).show();
        }

    }

    public static MainActivity getInstance() {
        return mainActivityRunningInstance;
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (getExternalFilesDir(mSelectedLanguage) != null) {
            mCategoriesAdapter.replaceData(listDownloadedCategories());
        } else {
            Toast.makeText(this, "Check external storage and update Google play services", Toast.LENGTH_LONG).show();
        }
    }


    //check that database uses only downloaded categories
    private List<Category> listDownloadedCategories() {
        Realm realm = Realm.getDefaultInstance();
        RealmQuery<Category> query = realm.where(Category.class).equalTo("downloaded", true);
        List<Category> savedList = query.findAll();
        if (savedList.size() != 0) {
            progressDialog.dismiss();
        }
        for (final Category category : savedList) {
            int numQuest = 0;
            String nameOfFolder = category.getAlias();
            File file = new File(getExternalFilesDir(mSelectedLanguage), nameOfFolder);
            if (file.listFiles() != null) {
                for (File f : file.listFiles()) {
                    if (f.isDirectory()) {
                        numQuest++;
                    }
                }
                Log.d(TAG, category.getAlias() + " " + String.valueOf(numQuest));
            }
        }
        return savedList;
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }

    public void updateUI() {
        mCategoriesAdapter.replaceData(listDownloadedCategories());
    }
}
