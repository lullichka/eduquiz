package com.daoris.eduquiz.data;


public interface DownloadInterface {

  interface LoadFilesCallback {
    void onFilesLoaded();
  }

  interface FilesDeletedCallback {
    void onFilesDeleted();
  }
}


