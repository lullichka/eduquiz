package com.daoris.eduquiz.data;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.daoris.eduquiz.EduQuizApp;
import com.daoris.eduquiz.R;
import com.daoris.eduquiz.model.Category;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import io.realm.Realm;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class DownloadIntentService extends IntentService {


    public static final String CATEGORY = "category";
    public static final String NUMBER_OF_QUESTION = "number_of_questions";

    private static final String TAG = DownloadIntentService.class.getSimpleName();
    private static final String STORAGE = "gs://eduquiz-964e6.appspot.com/";
    private static final String APP_PREFERENCES = "settings";
    private static final String LANGUAGE = "language";

    public DownloadIntentService() {
        super("DownloadIntentService");

    }

    @Override
    protected void onHandleIntent(Intent intent) {
        SharedPreferences preferences = getApplicationContext().getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
        String language = preferences.getString(LANGUAGE, this.getResources().getString(R.string.default_language).toLowerCase());
        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference storageReference = storage.getReferenceFromUrl(STORAGE).child(language);
        if (intent != null) {
            final String category = intent.getStringExtra(CATEGORY);
            final String numberOfQuestion = intent.getStringExtra(NUMBER_OF_QUESTION);

            //get the rest of files(questions)
            final String[] listOfPaths = getApplicationContext().getResources().getStringArray(R.array.list_of_paths);
            final String[] listOfPathsOnDisk = getApplicationContext()
                    .getResources().getStringArray(R.array.list_of_path_on_disk);
            final String[] listOfFilenames = getApplicationContext()
                    .getResources().getStringArray(R.array.list_of_filenames);
            for (int i = 0; i < listOfPaths.length; i++) {
                StringBuilder pathToFile = new StringBuilder("/")
                        .append(category).append("/")
                        .append(numberOfQuestion).append("/")
                        .append(listOfPaths[i]);
                Log.d(TAG, pathToFile.toString());
                final StringBuilder pathOnDisk = new StringBuilder("/").append(language).append("/")
                        .append(category).append("/").append(numberOfQuestion)
                        .append("/").append(listOfPathsOnDisk[i]);
                final String filename = listOfFilenames[i];
                final int finalI = i;
                storageReference.child(pathToFile.toString()).getDownloadUrl()
                        .addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {
                                Call<ResponseBody> call = EduQuizApp.getApi().downloadFileWithDynamicUrlSync(uri.toString());
                                call.enqueue(new Callback<ResponseBody>() {
                                    @Override
                                    public void onResponse(Call<ResponseBody> call,
                                                           Response<ResponseBody> response) {
                                        if (response.isSuccessful()) {
                                            Log.d(TAG, "server contacted and has file");
                                            boolean writtenToDisk = writeResponseBodyToDisk(response.body(),
                                                    getApplicationContext(), pathOnDisk.toString(), filename);
                                            Log.d(TAG, "file download was a success? " + writtenToDisk);
                                            if (finalI == listOfPaths.length - 1) {
                                                handleActionDownload(category, numberOfQuestion);
                                            }
                                        } else {
                                            Log.d(TAG, "server contact failed");
                                            Toast.makeText(getApplicationContext(), "File downloading failed, check Internet connection", Toast.LENGTH_LONG).show();
                                        }

                                    }

                                    @Override
                                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                                        Log.e(TAG, t.getMessage());
                                    }
                                });
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        Log.e(TAG, exception.getMessage());
                        Toast.makeText(getApplicationContext(), "File downloading failed", Toast.LENGTH_LONG).show();
                    }
                });

            }

        }
    }


    private static boolean writeResponseBodyToDisk(ResponseBody body, Context context, String filepath,
                                                   String filename) {
        if (isExternalStorageAvailable() && !isExternalStorageReadOnly()) {
            try {
                File file = new File(context.getExternalFilesDir(filepath), filename);
                InputStream inputStream = null;
                OutputStream outputStream = null;
                try {
                    byte[] fileReader = new byte[4096];
                    long fileSize = body.contentLength();
                    long fileSizeDownloaded = 0;
                    inputStream = body.byteStream();
                    outputStream = new FileOutputStream(file);
                    while (true) {
                        int read = inputStream.read(fileReader);
                        if (read == -1) {
                            break;
                        }
                        outputStream.write(fileReader, 0, read);
                        fileSizeDownloaded += read;
                    }
                    outputStream.flush();
                    return true;
                } catch (IOException e) {
                    return false;
                } finally {
                    if (inputStream != null) {
                        inputStream.close();
                    }
                    if (outputStream != null) {
                        outputStream.close();
                    }
                }
            } catch (IOException e) {
                return false;
            }
        } else return false;
    }

    public static boolean isExternalStorageReadOnly() {
        String extStorageState = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED_READ_ONLY.equals(extStorageState);
    }

    public static boolean isExternalStorageAvailable() {
        String extStorageState = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(extStorageState);
    }


    private void handleActionDownload(final String category, final String numberOfQuestion) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Category category1 = realm.where(Category.class)
                        .equalTo("alias", category)
                        .findFirst();
                category1.setLastDownloadedQuestion(Integer.parseInt(numberOfQuestion));
            }
        });
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(DownloadCompleteReceiver.ACTION_DOWNLOAD);
        broadcastIntent.putExtra(CATEGORY, category);
        broadcastIntent.putExtra(NUMBER_OF_QUESTION, numberOfQuestion);
        sendBroadcast(broadcastIntent);


    }
}
