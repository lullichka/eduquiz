package com.daoris.eduquiz.data;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.daoris.eduquiz.EduQuizApp;
import com.daoris.eduquiz.R;
import com.daoris.eduquiz.model.Category;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmQuery;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class FileDownloadUtil implements DownloadInterface {
  private static final String TAG = FileDownloadUtil.class.getSimpleName();
  private static final String STORAGE = "gs://eduquiz-964e6.appspot.com/";
  private static final String APP_PREFERENCES = "settings";
  private static final String LANGUAGE = "language";
  public static final String CATEGORY = "category";
  public static final String NUMBER_OF_QUESTION = "number_of_questions";


  private static void fileDownload(final Context context, final String category, int numberofquestion) {
    SharedPreferences preferences = context.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
    String language = preferences.getString(LANGUAGE, context.getResources().getString(R.string.default_language).toLowerCase());
    FirebaseStorage storage = FirebaseStorage.getInstance();
    final StorageReference storageRef = storage.getReferenceFromUrl(STORAGE).child(language);

    ;
    //get the image for category
    StringBuilder pathToImgCat = new StringBuilder("/");
    pathToImgCat.append("/").append(category).append("/image_category.jpg");
    Log.d(TAG, "Path to images in storage is " + pathToImgCat.toString());
    final StringBuilder pathToImgOnDisk = new StringBuilder("/").append(language).append("/").append(category);
    storageRef.child(pathToImgCat.toString()).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
      @Override
      public void onSuccess(Uri uri) {
        Call<ResponseBody> call = EduQuizApp.getApi().downloadFileWithDynamicUrlSync(uri.toString());
        call.enqueue(new Callback<ResponseBody>() {
          @Override
          public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
            if (response.isSuccessful()) {
              boolean writtenToDisk = writeResponseBodyToDisk(response.body(),
                  context, pathToImgOnDisk.toString(), "image_category.jpg");
            } else {
              Log.d(TAG, "server contact failed");
              Toast.makeText(context, "File downloading failed", Toast.LENGTH_LONG).show();
            }
          }

          @Override
          public void onFailure(Call<ResponseBody> call, Throwable t) {
            Log.e(TAG, t.getMessage());
            Toast.makeText(context, "File downloading failed", Toast.LENGTH_LONG).show();
          }
        });
      }
    }).addOnFailureListener(new OnFailureListener() {
      @Override
      public void onFailure(@NonNull Exception e) {
        Log.e(TAG, e.getMessage());
      }
    });
    //get the rest of files(questions)
    String[] listOfPaths = context.getResources().getStringArray(R.array.list_of_paths);
    final String[] listOfPathsOnDisk = context
        .getResources().getStringArray(R.array.list_of_path_on_disk);
    final String[] listOfFilenames = context
        .getResources().getStringArray(R.array.list_of_filenames);
    for (int k = 1; k < numberofquestion + 1; k++) {
      for (int i = 0; i < listOfPaths.length; i++) {
        StringBuilder pathToFile = new StringBuilder("/")
            .append(category).append("/")
            .append(k).append("/")
            .append(listOfPaths[i]);
        Log.d(TAG, pathToFile.toString());
        final StringBuilder pathOnDisk = new StringBuilder("/").append(language).append("/")
            .append(category).append("/").append(k)
            .append("/").append(listOfPathsOnDisk[i]);
        final String filename = listOfFilenames[i];
        storageRef.child(pathToFile.toString()).getDownloadUrl()
            .addOnSuccessListener(new OnSuccessListener<Uri>() {
              @Override
              public void onSuccess(Uri uri) {
                Call<ResponseBody> call = EduQuizApp.getApi().downloadFileWithDynamicUrlSync(uri.toString());
                call.enqueue(new Callback<ResponseBody>() {
                  @Override
                  public void onResponse(Call<ResponseBody> call,
                                         Response<ResponseBody> response) {
                    if (response.isSuccessful()) {
                      boolean writtenToDisk = writeResponseBodyToDisk(response.body(),
                          context, pathOnDisk.toString(), filename);
                    } else {
                      Toast.makeText(context, "File downloading failed, check Internet connection", Toast.LENGTH_LONG).show();
                    }
                  }

                  @Override
                  public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Log.e(TAG, t.getMessage());
                  }
                });
              }
            }).addOnFailureListener(new OnFailureListener() {
          @Override
          public void onFailure(@NonNull Exception exception) {
            Log.e(TAG, exception.getMessage());
            Toast.makeText(context, "File downloading failed", Toast.LENGTH_LONG).show();
          }
        });
      }
    }
  }

  private static boolean writeResponseBodyToDisk(ResponseBody body, Context context, String filepath,
                                                 String filename) {
    if (isExternalStorageAvailable() && !isExternalStorageReadOnly()) {
      try {
        File file = new File(context.getExternalFilesDir(filepath), filename);
        InputStream inputStream = null;
        OutputStream outputStream = null;
        try {
          byte[] fileReader = new byte[4096];
          long fileSizeDownloaded = 0;
          inputStream = body.byteStream();
          outputStream = new FileOutputStream(file);
          while (true) {
            int read = inputStream.read(fileReader);
            if (read == -1) {
              break;
            }
            outputStream.write(fileReader, 0, read);
            fileSizeDownloaded += read;
          }
          outputStream.flush();
          return true;
        } catch (IOException e) {
          return false;
        } finally {
          if (inputStream != null) {
            inputStream.close();
          }
          if (outputStream != null) {
            outputStream.close();
          }
        }
      } catch (IOException e) {
        return false;
      }
    } else return false;
  }

  public static boolean isExternalStorageReadOnly() {
    String extStorageState = Environment.getExternalStorageState();
    return Environment.MEDIA_MOUNTED_READ_ONLY.equals(extStorageState);
  }

  public static boolean isExternalStorageAvailable() {
    String extStorageState = Environment.getExternalStorageState();
    return Environment.MEDIA_MOUNTED.equals(extStorageState);
  }

  public static void downloadCommonFiles(final Context context, final String nameOfFile) {
    SharedPreferences preferences = context.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
    final String language = preferences.getString(LANGUAGE, context.getResources().getString(R.string.default_language).toLowerCase());
    FirebaseStorage storage = FirebaseStorage.getInstance();
    final StorageReference storageRef = storage.getReferenceFromUrl(STORAGE).child(language);

    storageRef.child(nameOfFile).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
      @Override
      public void onSuccess(Uri uri) {
        Call<ResponseBody> call = EduQuizApp.getApi().downloadFileWithDynamicUrlSync(uri.toString());
        call.enqueue(new Callback<ResponseBody>() {
          @Override
          public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
            if (response.isSuccessful()) {
              boolean writtenToDisk = writeResponseBodyToDisk(response.body(),
                  context, language, nameOfFile);
            } else {
              Toast.makeText(context, "File downloading failed", Toast.LENGTH_LONG).show();
            }
          }

          @Override
          public void onFailure(Call<ResponseBody> call, Throwable t) {
            Log.e(TAG, t.getMessage());
            Toast.makeText(context, "File downloading failed", Toast.LENGTH_LONG).show();
          }
        });
      }
    });

  }

  public static void getListOfCategories(final Context context, final LoadFilesCallback callback) {
    SharedPreferences preferences = context.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
    String language = preferences.getString(LANGUAGE,
        context.getResources().getString(R.string.default_language).toLowerCase());
    DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference(language);
    databaseReference.addValueEventListener(new ValueEventListener() {
      @Override
      public void onDataChange(DataSnapshot dataSnapshot) {
        final List<Category> categories = new ArrayList<>();
        for (DataSnapshot categoryshapshot : dataSnapshot.getChildren()) {
          final Category category = categoryshapshot.getValue(Category.class);
          if (!checkIfExists(category.getAlias())) {
            categories.add(category);
          }
        }
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
          @Override
          public void execute(Realm realm) {
            realm.copyToRealmOrUpdate(categories);
          }
        });
        for (Category cat : categories) {
          downloadImagesOfCategories(context, cat.getAlias());
        }
        Intent downloadIntent = new Intent(context, DownloadIntentService.class);
        Log.d(TAG, "Service fired");
        downloadIntent.putExtra(CATEGORY, categories.get(0).getAlias());
        downloadIntent.putExtra(NUMBER_OF_QUESTION, "1");
        context.startService(downloadIntent);
        callback.onFilesLoaded();
      }

      @Override
      public void onCancelled(DatabaseError databaseError) {
        Log.e(TAG, databaseError.getMessage());
      }
    });
  }

  private static void downloadImagesOfCategories(final Context context, String alias) {
    SharedPreferences preferences = context.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
    final String language = preferences.getString(LANGUAGE, context.getResources().getString(R.string.default_language).toLowerCase());
    FirebaseStorage storage = FirebaseStorage.getInstance();
    final StorageReference storageRef = storage.getReferenceFromUrl(STORAGE).child(language).child(alias);

    final String pathOnDisk = "/" + language + "/" + alias;
    String pathToStorage = "/image_category.jpg";
    storageRef.child(pathToStorage).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
      @Override
      public void onSuccess(Uri uri) {
        Call<ResponseBody> call = EduQuizApp.getApi().downloadFileWithDynamicUrlSync(uri.toString());
        call.enqueue(new Callback<ResponseBody>() {
          @Override
          public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
            if (response.isSuccessful()) {
              boolean writtenToDisk = writeResponseBodyToDisk(response.body(),
                  context, pathOnDisk, "image_category.jpg");
            } else {
              Log.d(TAG, "server contact failed");
              Toast.makeText(context, "File downloading failed", Toast.LENGTH_LONG).show();
            }
          }

          @Override
          public void onFailure(Call<ResponseBody> call, Throwable t) {
            Log.e(TAG, t.getMessage());
            Toast.makeText(context, "File downloading failed", Toast.LENGTH_LONG).show();
          }
        });
      }
    });
  }


  //copy only new categories (by name)
  private static boolean checkIfExists(String alias) {
    Realm realm = Realm.getDefaultInstance();
    RealmQuery<Category> query = realm.where(Category.class)
        .equalTo("alias", alias);
    return query.count() != 0;
  }

  public static void clearFilesAndDownloadAgain(Context context, FilesDeletedCallback callback) {
    SharedPreferences preferences = context.getSharedPreferences(APP_PREFERENCES,
        Context.MODE_PRIVATE);
    String language = preferences.getString(LANGUAGE,
        context.getResources().getString(R.string.default_language).toLowerCase());
    File dir = new File(Environment.getExternalStorageDirectory()+"/"+language);
    if (dir.isDirectory()) {
      String[] children = dir.list();
      for (String aChildren : children) {
        new File(dir, aChildren).delete();
      }
    }
    Realm realm = Realm.getDefaultInstance();
    realm.executeTransaction(new Realm.Transaction() {
      @Override
      public void execute(Realm realm) {
        realm.deleteAll();
      }
    });
    callback.onFilesDeleted();
  }
}

