package com.daoris.eduquiz.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.daoris.eduquiz.R;
import com.daoris.eduquiz.data.DownloadInterface;
import com.daoris.eduquiz.data.FileDownloadUtil;
import com.daoris.eduquiz.model.User;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class LaunchingActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener,
        View.OnClickListener {

    private static final String APP_PREFERENCES = "settings";
    private static final String USER_NAME = "username";
    private static final String LANGUAGE = "language";
    private static final String USER_ID = "uniqueId";
    private static final String DESCRIPTION = "/description.mp3";
    private static final String RIGHT = "/right.mp3";
    private static final String WRONG = "/wrong.mp3";


    private static final String TAG = LaunchingActivity.class.getSimpleName();
    private static final String USER_CHILD = "USERS";
    private static final int PERMISSION_REQUEST_CODE = 1;

    private SharedPreferences mSettings;
    private EditText mEditText;
    private String mSelectedLanguage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch);
        Spinner mSpinner = (Spinner) findViewById(R.id.spinner);
        mEditText = (EditText) findViewById(R.id.etName);
        Button mButton = (Button) findViewById(R.id.button);
        mSettings = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.languages, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinner.setAdapter(adapter);
        mSpinner.setOnItemSelectedListener(this);
        mButton.setOnClickListener(this);


    }

    public void requestMultiplePermissions() {
        ActivityCompat.requestPermissions(this,
                new String[]{
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                },
                PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST_CODE && grantResults.length == 2) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                    grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                doDownloadStaff();
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void doDownloadStaff() {
        //download common mp3 files
        FileDownloadUtil.downloadCommonFiles(getApplicationContext(), DESCRIPTION);
        FileDownloadUtil.downloadCommonFiles(getApplicationContext(), RIGHT);
        FileDownloadUtil.downloadCommonFiles(getApplicationContext(), WRONG);
        //download categories from Firebase database, then files from Firebase storage
        FileDownloadUtil.getListOfCategories(getApplicationContext(), new DownloadInterface.LoadFilesCallback() {
            @Override
            public void onFilesLoaded() {
                Log.d(TAG, "Sucessfully loaded");
                Intent intent = new Intent(LaunchingActivity.this, WelcomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        mSelectedLanguage = (String) adapterView.getItemAtPosition(i);
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
        mSelectedLanguage = getResources().getString(R.string.default_language).toLowerCase();
    }

    @Override
    public void onClick(View view) {
        String username = mEditText.getText().toString().trim();
        if (view.getId() == R.id.button) {
            if (TextUtils.isEmpty(username)) {
                Toast.makeText(this, getResources().getString(R.string.name_cant_be_empty), Toast.LENGTH_LONG).show();
            } else {
                //create new User with unique id in Firebase
                User user = new User(username);
                DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference(USER_CHILD);
                DatabaseReference reference = databaseReference.push();
                String userId = reference.getKey();
                user.setId(userId);
                reference.setValue(user);
                //save user settings to shared preference
                SharedPreferences.Editor editor = mSettings.edit();
                editor.putString(USER_NAME, username);
                editor.putString(LANGUAGE, mSelectedLanguage.toLowerCase());
                editor.putString(USER_ID, userId);
                editor.apply();
                requestMultiplePermissions();

            }
        }
    }
}
