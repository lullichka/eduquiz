package com.daoris.eduquiz.model;


import io.realm.RealmObject;

public class Category extends RealmObject {
    private String alias;
    private int id;
    private String name;
    private int numQuest;
    private boolean downloaded;
    private int lastDownloadedQuestion;


    public Category() {
        // Default constructor required for calls to DataSnapshot.getValue(Category.class)
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumQuest() {
        return numQuest;
    }

    public void setNumQuest(int numQuest) {
        this.numQuest = numQuest;
    }


    public boolean isDownloaded() {
        return downloaded;
    }

    public void setDownloaded(boolean downloaded) {
        this.downloaded = downloaded;
    }

    public int getLastDownloadedQuestion() {
        return lastDownloadedQuestion;
    }

    public void setLastDownloadedQuestion(int lastDownloadedQuestion) {
        this.lastDownloadedQuestion = lastDownloadedQuestion;
    }
}
